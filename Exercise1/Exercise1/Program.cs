﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise1
{
    class Program
    {
        static void Main(string[] args)
        {
            string input;
           // int count = 0;

            Console.WriteLine("Hello! Welcome to InfoPerson!");
            Console.WriteLine(" ");

            InfoPerson person = new InfoPerson();

            Console.WriteLine("Type your first name.");
            person.firstName = Console.ReadLine();

            Console.WriteLine("Type your last name.");
            person.lastName = Console.ReadLine();

            Console.WriteLine("How old are you?");
            person.age = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("What is your job/work?");
            person.occupation = Console.ReadLine();

            Console.WriteLine("Month of your birth? (Please type the complete month name)");
            person.monthName = Console.ReadLine();

            Console.WriteLine("Day of your birth?");
            person.dayBirth = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Year of birth.");
            person.year = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Your weight?");
            person.weight = float.Parse(Console.ReadLine());

            Console.WriteLine(" ");
            Console.WriteLine("YOUR INFOPERSON PROFILE!");
            Console.WriteLine("Name: "+  person.firstName + " " + person.lastName);
            Console.WriteLine("Age: " + person.age);
            Console.WriteLine("Occupation: " + person.occupation);
            Console.WriteLine("Birthdate: " + person.monthName + "."+ person.dayBirth + "," + person.year);
            Console.WriteLine("Weight: " + person.weight, "lbs");

            Console.Read();

        }
    }
}
